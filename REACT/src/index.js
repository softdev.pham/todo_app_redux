import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux' // Có sử dụng Reducers nên cần Provider để lưu Store
import { createStore, applyMiddleware, compose } from 'redux' // Do chúng ta có sử dụng Reducers nên phải tạo Store, còn Middleware để ngăn cho các trường hợp như actions trả về là Promise chẳng hạn.
import { BrowserRouter } from 'react-router-dom' // Sử dụng cái này để tạo Routes nhé
import rootReducer from './reducers/rootReduce'
import App from './App';
import serviceWorker from './serviceWorker';
import thunk from 'redux-thunk';

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  rootReducer,
  composeEnhancer(applyMiddleware(thunk))
);

ReactDOM.render(
<Provider store={store}>
  <BrowserRouter>
    <App />
  </BrowserRouter>
</Provider>
, document.getElementById('root'));
serviceWorker();
