import { ADD_TODO, REMOVE_TODO, TOGGLE_TODO } from '../constants/ActionTypes'

const INITIAL_DATA = []

const TodoReducers = (state=INITIAL_DATA, action) => {
    var date = new Date();
    var time = date.getTime();
    switch (action.type){
        case ADD_TODO:
            return [
                ...state,{
                    id: action.id ?? time,
                    text: action.text,
                    completed: false,
                }
            ]
        case REMOVE_TODO:
            return state.filter( (todo) => {
                return todo.id !== action.id
            })

        case TOGGLE_TODO:
            return state.map(todo =>
                (todo.id === action.id)
                  ? {...todo, completed: !action.completed}
                  : todo
                )
        default:
        return state
    }
}

export default TodoReducers