import * as types from '../constants/ActionCounts'

export function fetchUsers(dispatch) {
    fetch('https://reqres.in/api/users')
    .then(res => res.json())
    .then(res => dispatch({
        type: types.FETCH_USERS,
        payload:res.data
    }))
}