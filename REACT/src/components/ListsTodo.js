import React, { Component } from 'react'
export class ListsTodo extends Component {
    constructor(props) {
        super(props)

        this.state = {

        }
    }

    handleRemove = (id) => {
        this.props.removeTodo(id);
    }

    handleToggle = (id, completed) => {
        this.props.toggleCheckTodo(id, completed);
    }

    render() {
        const { todos } = this.props;
        return (
            <div className="form-group row">
                <div className="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                {todos.length !== 0 ? (
                <table
                    style={{ marginTop: "60px" }}
                    className="table table-hover table-dark"
                >
                    <thead>
                    <tr>
                        <th scope="col">Todos</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    {todos.map(todo => (
                        <tr key={todo.id}>
                        <td
                            style={{
                                textDecoration: todo.completed ? "line-through" : "none"
                            }}
                        >
                            {todo.text} {todo.completed === true ? "(completed)" : ""}
                        </td>
                        <td>
                            <span
                                className="fas fa-minus-circle"
                                style={{
                                    color: "white",
                                    fontSize: "20pt",
                                    marginRight: "20px"
                                }}
                                onClick={() => this.handleRemove(todo.id)}
                            />
                            <span
                                className="fas fa-check-circle"
                                style={{ color: "white", fontSize: "20pt" }}
                                onClick={() => this.handleToggle(todo.id, todo.completed)}
                            />
                        </td>
                        </tr>
                    ))}
                    </tbody>
                </table>
                ) : (
                <div
                    style={{ marginTop: "50px" }}
                    className="col-xs-12 col-sm-12"
                >
                    <div className="alert alert-danger" role="alert">
                    Todo List is empty or Filter results show no results
                    </div>
                </div>
                )}{" "}
                </div>
            </div>
        )
    }
}

export default ListsTodo
