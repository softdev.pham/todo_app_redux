import React, { Component } from 'react'
import { Link,withRouter } from 'react-router-dom'

class Menu extends Component {
    render() {
        const path = this.props.location.pathname;
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light" style={{
                marginBottom: "50px"
            }}>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav">
                        <li className={"nav-item " + (path === '/' ? 'active' : '')}>
                            <Link 
                                className="nav-link" 
                                to="/"
                            >Todo App</Link>
                        </li>
                        {/* <li className={"nav-item " + (path === '/count' ? 'active' : '')}>
                            <Link className="nav-link" to="/count">Count App</Link>
                        </li> */}
                    </ul>
                </div>
            </nav>
        )
    }
}

// withRouter will support get the values of Route
export default withRouter(Menu);
