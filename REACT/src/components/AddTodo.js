import React, { Component } from 'react'

export default class AddTodo extends Component {
    constructor(props) {
        super(props);

        this.state = {
            text: ''
        }
    }
    handleAddTodo = (e) => {
        e.preventDefault();
        const nameInput = this.refs.nameInput.value;
        this.setState ( (state) => {
            return {
                text: nameInput
            }
        })

        this.props.addTodo(nameInput);
        this.refs.nameInput.value = '';
    }
    handleCancel = (e) => {
        e.preventDefault();
        this.refs.nameInput.value = '';
    }

    handleOnKey = (event) => {
        if (event.charCode === 13) {
            this.handleAddTodo(event);
        }
    }

    render() {
        return (
            <div className="form-group row">
                <div className="col-sm-10">
                    <input 
                        type="text" 
                        className="form-control" 
                        id="nameInput" 
                        placeholder="add todo here"
                        ref="nameInput"
                        onKeyPress={this.handleOnKey}
                    />
                    <button 
                        type="button"  
                        style={{marginTop: "25px", marginRight: "15px"}} 
                        className="btn btn-danger"
                        onClick={this.handleCancel}
                        >Cancel</button>
                    <button 
                        type="button" 
                        style={{marginTop: "25px"}} 
                        className="btn btn-success"
                        onClick={this.handleAddTodo}
                    >Add Todo</button>
                </div>
            </div>
        )
    }
}
