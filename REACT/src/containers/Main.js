import React, { Component } from 'react'
import { Switch, Route } from 'react-router-dom'
import TodoApp  from '../containers/TodoApp'
import CountApp  from '../containers/CountApp'

export class Main extends Component {
    render() {
        return (
            <Switch>
                <Route exact path="/" component={TodoApp} />
                {/* <Route exact path="/count" component={CountApp} /> */}
            </Switch>
        )
    }
}

export default Main

