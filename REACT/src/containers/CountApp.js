import React, { Component } from 'react'
import { connect } from 'react-redux'
import { fetchUsers } from '../actions/ActionsCount'
import {bindActionCreators} from 'redux'

export class CountApp extends Component {

    componentWillMount() {
        // this.props.fetchUsers();
    }

    render() {
        return (
            <div>
                This is Count app!
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    
})

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        fetchUsers: fetchUsers,
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(CountApp)
