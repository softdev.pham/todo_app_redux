import React, { Component } from 'react'
import {bindActionCreators} from 'redux'
import { connect } from 'react-redux'
import { addTodo, removeTodo, toggleCheckTodo } from '../actions/ActionsTodo'
import AddTodo from '../components/AddTodo'
import ListsTodo from '../components/ListsTodo'


export class TodoApp extends Component {
    constructor(props)
    {
        super(props);

        this.state = {

        }
    }

    render() {
        const { add_todo, todos, remove_todo, toggle_check_todo } = this.props;
        return (
            <div>
                <AddTodo addTodo={add_todo}/>
                <ListsTodo 
                    todos={todos} 
                    removeTodo={remove_todo} 
                    toggleCheckTodo={toggle_check_todo}
                />
            </div>
        )
    }
}
// 
const mapStateToProps = (state) => {
    return {
        todos: state.TodoReducers
    };
}

// Bring Action to Props 
const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        add_todo: addTodo,
        remove_todo: removeTodo,
        toggle_check_todo: toggleCheckTodo
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(TodoApp)
