import React from 'react'
import { Switch, Route } from 'react-router-dom'
import './App.css';
import Main  from './containers/Main'
import Menu  from './components/Menu'

function App() {
  return (
    <div className="App">
      <div className="container" style={{ marginTop: "80px"}} >
        <div className="row">
          <div className="col-lg-10 offset-lg-2 col-md-10 col-sm-12 col-xs-12">
            <Menu/>
          </div>
          <div className="col-lg-10 offset-lg-2 col-md-10 col-sm-12 col-xs-12">
            <Main />
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
